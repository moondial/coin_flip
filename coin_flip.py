##Base Scratch file to iterate from
##Use scratch1 to iterate with

import random



print("----------------------------")
print("      Coin Flip Bet v3")
print("----------------------------")

user = input("What is your name? ")


calls =  ['heads', 'tails']
flip = random.choice(calls)

call1 = input("What do you call? [heads, tails]: ")
call1 = call1.lower().strip()

if call1 not in calls:
    print(f"Sorry {user}, {call1} is not a valid call.")

# Heads
#     heads -> win
#     tails -> lose
# Tails
#     tails -> win
#     heads -> lose
winner = None
loser = None

# Call heads & win
if call1 == 'heads' and flip == 'heads':
    winner = user
    print(f"{user} calls {call1} and won that bet!")

# Call heads & lose
elif call1 == 'heads' and flip == 'tails':
    loser = user
    print(f"{user} calls {call1}.")
    print(f"The coin flip landed {flip}!")
    print("The house took that one.")

# Call tails and win
if call1 == 'tails' and flip == 'tails':
    winner = user
    print(f"{user} calls {call1} and won that bet!")

# call tails and lose
elif call1 == 'tails' and flip == 'heads':
    loser = user
    print(f"{user} calls {call1}.")
    print(f"The coin flip landed {flip}!")
    print("The house took that one.")


